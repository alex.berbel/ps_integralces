# Accept Social Coin payments

## About

Accept Social Coin payments with IntegralCES or CES


## How works

You can install like other module and work with your currencies, but the module only do a validation for a order.. if your customers don't put their integralCES account or CES account in their profile is a bit difficult to get pay.

You can add this new fields in the customer profile use another module overriding the prestashop in the folder override

please follow the instructions or contact with me al-demon@3toques.es or in komun.org

this https://gitlab.com/Al-Demon/ps-social-currencies or this https://gitlab.com/Al-Demon/ps-custom-register

Thanks



## Contributing

PrestaShop modules are open-source extensions to the PrestaShop e-commerce solution. Everyone is welcome and even encouraged to contribute with their own improvements.

### Requirements

Contributors **must** follow the following rules:

* **Make your Pull Request on the "dev" branch**, NOT the "master" branch.
* Do not update the module's version number.
* Follow [the coding standards][1].
* You need a special register field in your prestashop, please follow this instructions: https://prestacraft.com/adding-new-fields-to-the-registration-form-in-prestashop-1-7/#


### Process in details

Contributors wishing to edit a module's files should follow the following process:

1. Create your GitHub account, if you do not have one already.
2. Fork the ps_cashondelivery project to your GitHub account.
3. Clone your fork to your local machine in the ```/modules``` directory of your PrestaShop installation.
4. Create a branch in your local clone of the module for your changes.
5. Change the files in your branch. Be sure to follow [the coding standards][1]!
6. Push your changed branch to your fork in your GitHub account.
7. Create a pull request for your changes **on the _'dev'_ branch** of the module's project. Be sure to follow [the commit message norm][2] in your pull request. If you need help to make a pull request, read the [Github help page about creating pull requests][3].
8. Wait for one of the core developers either to include your change in the codebase, or to comment on possible improvements you should make to your code.

That's it: you have contributed to this open-source project! Congratulations!

[1]: http://doc.prestashop.com/display/PS16/Coding+Standards
[2]: http://doc.prestashop.com/display/PS16/How+to+write+a+commit+message
[3]: https://help.github.com/articles/using-pull-requests

